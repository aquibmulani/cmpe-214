Hello!!

1. For compiling the project, the following command is to be run in the folder where the files are locateed : The process_images.cu is our file for filter implementations in CUDA and cascadeclassifiers.cpp is also another file for the face detection : 
nvcc process_images.cu cascadeclassifier.cpp -I/usr/local/include/ -L./../../release/lib/ -lopencv_objdetect -lopencv_imgproc -lopencv_core -lopencv_highgui -lopencv_imgcodecs -lopencv_videoio -lopencv_cudaobjdetect -lopencv_cudaimgproc -lopencv_cudawarping

2. To run the executable file after compilation, the following command is used:
./a.out --cascade ~/opencv/data/haarcascades_cuda/haarcascade_frontalface_default.xml --camera 0
In the above command ./a,out is the executable file 
~/opencv/data/haarcascades_cuda/haarcascade_frontalface_default.xml is the path for the trained cascade file.

The files were compiled by us on Ubuntu 16.04.
3. Please make a folder with the name images so that the images after application of filters that are made after the face is detected are stored in that folder.

4. The files in the folder "Optimized_Kernels_for_filters" require an input image to apply the filter on.
   We have currently set the path to "images/lena_color.bmp"  . Please have an image with the name "lena_color" and extension .bmp to test those kernel implementations.
