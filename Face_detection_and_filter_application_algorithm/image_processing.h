#ifndef IMAGE_PROCESSING_H
#define IMAGE_PROCESSING_h

#include <stdio.h>
// #include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>
//#include <time.h>

 #define IMG_HEADER_SIZE 54
 #define COLOR_TBL_SIZE 1024
 #define WHITE   255
 #define BLACK   0
 #define THRESHOLD   100
 #define DEBUG 1
 #define MAX_PIXEL 255
//Types of Filter:
 #define BLACK_WHITE 1
 #define RGB_GRAY 2
 #define NEGATIVE 3
#define SEPHIA	4		

typedef struct {
   int height;
   int width;
   int bitDepth;
   int imgSize;
   int size;
}header_details_t;

  //double total_time;
  //clock_t start, end;

 void read_write_header(FILE *fIn,FILE *fOut);//This Function Reads Header from Input Image.
 void read_write_color_table(FILE *fIn,FILE *fOut,int bitDepth);//This Functoin reads and writes the color table.
//IMAGE PROCESSING APIS................................................................
 void apply_filter(FILE *fIn,FILE *fOut,int type);//This function converts an image into black_White depending upon threshod value.


#endif
