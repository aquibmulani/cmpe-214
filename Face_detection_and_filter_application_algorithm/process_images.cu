#include "image_processing.h"
header_details_t header_det={0};

__global__ void convert_rgb_to_sephia( unsigned  char *buffer,int size){
  int g_id = blockIdx.x * blockDim.x + threadIdx.x; 
 if(g_id<size/3){
   int pix_id=g_id*3;
   int r=0;
   int g=0;
   int b=0;
  r=(buffer[pix_id+0]*0.393)+(buffer[pix_id+1]*0.769)+(buffer[pix_id+2]*0.189);
  g=(buffer[pix_id+0]*0.349)+(buffer[pix_id+1]*0.686)+(buffer[pix_id+2]*0.168);
  b=(buffer[pix_id+0]*0.272)+(buffer[pix_id+1]*0.534)+(buffer[pix_id+2]*0.131);
  if(r>MAX_PIXEL){r=MAX_PIXEL;}
  if(g>MAX_PIXEL){g=MAX_PIXEL;}
  if(b>MAX_PIXEL){b=MAX_PIXEL;}	
  buffer[pix_id+0] = b;//R
  buffer[pix_id+1] = g;//G
  buffer[pix_id+2] = r;//B
  
 }  
} 
__global__ void convert_to_black_white( unsigned  char *buffer,int size){
	int g_id = blockIdx.x * blockDim.x + threadIdx.x;  
 	buffer[g_id] = (buffer[g_id]>THRESHOLD)? WHITE : BLACK;
}

__global__ void convert_rgb_to_grayscale( unsigned  char *buffer,int size){
   int g_id = blockIdx.x * blockDim.x + threadIdx.x; 
   if(g_id<size/3){
    int pix_id=g_id*3;
    int temp;
    temp = (buffer[pix_id+0]*0.3 )+(buffer[pix_id+1]*0.59)+(buffer[pix_id+2]*0.11);
    buffer[pix_id+0] = temp;
    buffer[pix_id+1] = temp;
    buffer[pix_id+2] = temp; 
   }  
}


__global__ void convert_to_negative( unsigned  char *buffer,int size){
   int g_id = blockIdx.x * blockDim.x + threadIdx.x; 
   int temp;
   temp=buffer[g_id];
   temp=255- buffer[g_id];
   buffer[g_id]=temp;  
}

void read_write_header(FILE *fIn,FILE *fOut){

 unsigned  char imgHeader[54];
 if(fIn == NULL)
   {
       printf("Unable to open image:read_write_header() \n");
       exit(EXIT_FAILURE);
   }
 
//......Read & write headerfile...
 fread(imgHeader,sizeof(unsigned char),54,fIn);
 fwrite(imgHeader,sizeof(unsigned char),54,fOut);
  header_det.height = *(int*)&imgHeader[22];
  header_det.width  = *(int*)&imgHeader[18];
  header_det.bitDepth = *(int *)&imgHeader[28];
  int img_size=(header_det.height) * (header_det.width)*3;
  header_det.imgSize = img_size;
  header_det.size = header_det.imgSize*sizeof(unsigned char);
 printf("imgSize: %d\n",header_det.imgSize);
 
}

void read_write_color_table(FILE *fIn,FILE *fOut,int bitDepth){
	unsigned  char colorTable[1024];
   	if(bitDepth<=8)
   	{
       fread(colorTable, sizeof(unsigned char),1024,fIn);
       fwrite(colorTable,sizeof(unsigned char),1024,fOut);
   	}
       printf(":read_write_color_table() \n");
}

void apply_filter(FILE *fIn,FILE *fOut,int type){
 	
	cudaError_t err = cudaSuccess;
 	read_write_header(fIn,fOut);
	read_write_color_table(fIn,fOut,header_det.bitDepth);
//......Allocate the host buffer:
     	unsigned  char *host_Buffer = (unsigned  char *)malloc(header_det.size);//buffer to process pixels
     	if (host_Buffer == NULL )
 	{
 	fprintf(stderr, "Failed to allocate host buffer.!\n");
 	exit(EXIT_FAILURE);
 	}
//......Read image_buffer into host.	
     fread(host_Buffer,sizeof(unsigned char),header_det.imgSize,fIn);
//......Initialise device_Buffer_for CUDA:
     unsigned  char *device_Buffer = NULL;
     err = cudaMalloc((void**)&device_Buffer,header_det.size);
     if (err != cudaSuccess)
     {
 	fprintf(stderr, "Failed to allocate device_Buffer (error code %s)!\n",
 	cudaGetErrorString(err));
 	exit(EXIT_FAILURE);
     }
 printf("Copy input data from the host memory to the CUDA device\n");

//-------------------HOST-TO-DEVICE-------------------------------------
 	err = cudaMemcpy(device_Buffer,host_Buffer,header_det.size,cudaMemcpyHostToDevice);
  	if (err != cudaSuccess)
  	{
  	fprintf(stderr, "Failed to copy Buffer from host to device (error code %s)!\n", cudaGetErrorString(err));
  	exit(EXIT_FAILURE);
 	}
//......CUDA Kernel Launch 
	int totalThreads =header_det.imgSize ;
 	int blockPerGrid =totalThreads/1024 ;
 	int threadPerBlock=1024;
	switch(type){
		case BLACK_WHITE:
		 //start=clock();
   		 convert_to_black_white<<<blockPerGrid,threadPerBlock>>>(device_Buffer,header_det.size);
   		 //end=clock();
	        break;
		case RGB_GRAY:
		 //start=clock();
		 convert_rgb_to_grayscale<<<blockPerGrid,threadPerBlock>>>(device_Buffer,header_det.imgSize);
		 //end=clock();
		break;
		case NEGATIVE:
		 //start=clock();
		 convert_to_negative<<<blockPerGrid,threadPerBlock>>>(device_Buffer,header_det.imgSize);
		 //end=clock();		
		break;
		case SEPHIA:
		 convert_rgb_to_sephia<<<blockPerGrid,threadPerBlock>>>(device_Buffer,header_det.imgSize);
		break;

	}
	
//......Calulate total time
	//total_time = ((double) (end - start));
        //printf("\nTime taken:%f\n", total_time);
        err = cudaGetLastError();
	if (err != cudaSuccess)
 	{
	fprintf(stderr, "Failed to launch convert_to_black_white kernel (error code %s)!\n",cudaGetErrorString(err));
	exit(EXIT_FAILURE);
 	}
//.....Copy Result from Device to Host....................
	 printf("Copy output data from the CUDA device to the host memory\n");
 	 err = cudaMemcpy(host_Buffer,device_Buffer,header_det.size,cudaMemcpyDeviceToHost);
	 if (err != cudaSuccess)
 	{ 
 	fprintf(stderr, "Failed to copy Buffer from device to host (error code %s)!\n", cudaGetErrorString(err));
 	exit(EXIT_FAILURE);
 	}

//......Write image Buffer to output file.......
 	fwrite(host_Buffer,sizeof(unsigned char), header_det.imgSize,fOut);
 	err = cudaFree(device_Buffer);
 	if (err != cudaSuccess)
 	{
	 fprintf(stderr, "Failed to free device Buffer (error code %s)!\n",cudaGetErrorString(err));
	 exit(EXIT_FAILURE);
 	}
 	free(host_Buffer);
 	err = cudaDeviceReset();
 	if (err != cudaSuccess)
 	{
	 fprintf(stderr, "Failed to deinitialize the device! error=%s\n", cudaGetErrorString(err));
	 exit(EXIT_FAILURE);
 	}
}


/*int main(void)
{ 

   FILE *fInput = fopen("images/goldhill.bmp","rb");
   FILE *fOutput = fopen("images/goldhill_neg.bmp","wb");
   apply_filter(fInput,fOutput,NEGATIVE);
 
 
  printf("Success\n");
  fclose(fInput);
  fclose(fOutput);
 return 0;
}*/
