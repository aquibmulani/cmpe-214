// 0.32 ms for the sequential type execution


#include "image_processing.h"
#include <cuda_runtime.h>

__global__ void
convert_to_negative( unsigned  char *buffer,unsigned char * output_buffer,int size)
{
  int g_id = blockIdx.x * blockDim.x + threadIdx.x; 
  int temp;
  temp=buffer[g_id];
  temp=255- buffer[g_id];
  output_buffer[g_id]=temp;
  
}

void apply_negative(FILE *fIn,FILE *fOut ){
 cudaError_t err = cudaSuccess;
 unsigned  char colorTable[1024];
 unsigned  char imgHeader[54];
 if(fIn == NULL)
   {
       printf("Unable to open image \n");
   }
  //Read & write headerfile...
 fread(imgHeader,sizeof(unsigned char),54,fIn);
 fwrite(imgHeader,sizeof(unsigned char),54,fOut);
  
   int height = *(int*)&imgHeader[22];
   int width  = *(int*)&imgHeader[18];
   int bitDepth = *(int *)&imgHeader[28];
   int imgSize = height * width*3;
   printf("imgSize: %d\n",imgSize);
   int size = imgSize*sizeof(unsigned char);

//Read and Write the colortable..
  if(bitDepth<=8)
   {
       fread(colorTable, sizeof(unsigned char),1024,fIn);
       fwrite(colorTable,sizeof(unsigned char),1024,fOut);
   }




// Allocate the host buffer:
  unsigned  char *host_Buffer = (unsigned  char *)malloc(size);//buffer to process pixels
    
 if (host_Buffer == NULL )
 {
 fprintf(stderr, "Failed to allocate host buffer.!\n");
 exit(EXIT_FAILURE);
 }
//Allocate output host buffer
 unsigned  char *h_B = (unsigned  char *)malloc(size);//buffer to process pixels
    
 if (h_B == NULL )
 {
 fprintf(stderr, "Failed to allocate host buffer.!\n");
 exit(EXIT_FAILURE);
 }

 //Read image_buffer into host.	
 fread(host_Buffer,sizeof(unsigned char),imgSize,fIn);

//------------------------------------------------------------CUDA Streams-------------------------------------------------------------

int segSize=imgSize/4;
 float millisecs=0;
 //int totalThreads =imgSize;
 int blockPerGrid =segSize/1024 ;
 int threadPerBlock=1024;
  //size_t i_size=imgSize;

cudaEvent_t start1, stop1;	
   cudaEventCreate(&start1);
   cudaEventCreate(&stop1);
cudaStream_t stream0,stream1,stream2,stream3;
cudaStreamCreate(&stream0);
cudaStreamCreate(&stream1);
cudaStreamCreate(&stream2);
cudaStreamCreate(&stream3);



unsigned  char *d_A0,*d_B0;
unsigned  char *d_A1,*d_B1;
unsigned  char *d_A2,*d_B2;
unsigned  char *d_A3,*d_B3;




err = cudaMalloc((void**)&d_A0,segSize* sizeof(unsigned  char));
err = cudaMalloc((void**)&d_B0,segSize* sizeof(unsigned  char));

err = cudaMalloc((void**)&d_A1,segSize* sizeof(unsigned  char));
err = cudaMalloc((void**)&d_B1,segSize* sizeof(unsigned  char));

err = cudaMalloc((void**)&d_A2,segSize* sizeof(unsigned  char));
err = cudaMalloc((void**)&d_B2,segSize* sizeof(unsigned  char));

err = cudaMalloc((void**)&d_A3,segSize* sizeof(unsigned  char));
err = cudaMalloc((void**)&d_B3,segSize* sizeof(unsigned  char));


cudaEventRecord(start1); 

 cudaMemcpyAsync(d_A0,host_Buffer+(0*segSize),segSize * sizeof(unsigned  char),cudaMemcpyHostToDevice,stream0 );
 cudaMemcpyAsync(d_A1,host_Buffer+(1*segSize),segSize * sizeof(unsigned  char),cudaMemcpyHostToDevice,stream1 );
 cudaMemcpyAsync(d_A2,host_Buffer+(2*segSize),segSize * sizeof(unsigned  char),cudaMemcpyHostToDevice,stream2 );
 cudaMemcpyAsync(d_A3,host_Buffer+(3*segSize),segSize * sizeof(unsigned  char),cudaMemcpyHostToDevice,stream3 );


 convert_to_negative<<<blockPerGrid,threadPerBlock,0,stream0>>>(d_A0,d_B0,segSize);
convert_to_negative<<<blockPerGrid,threadPerBlock,0,stream1>>>(d_A1,d_B1,segSize);
convert_to_negative<<<blockPerGrid,threadPerBlock,0,stream2>>>(d_A2,d_B2,segSize);
convert_to_negative<<<blockPerGrid,threadPerBlock,0,stream3>>>(d_A3,d_B3,segSize);
    
	




 cudaMemcpyAsync(h_B+(0*segSize),d_B0,segSize * sizeof(unsigned  char),cudaMemcpyDeviceToHost,stream0 );
 cudaMemcpyAsync(h_B+(1*segSize),d_B1,segSize * sizeof(unsigned  char),cudaMemcpyDeviceToHost,stream1 );
 cudaMemcpyAsync(h_B+(2*segSize),d_B2,segSize * sizeof(unsigned  char),cudaMemcpyDeviceToHost,stream2 );
 cudaMemcpyAsync(h_B+(3*segSize),d_B3,segSize * sizeof(unsigned  char),cudaMemcpyDeviceToHost,stream3 );




cudaEventRecord(stop1); 
cudaEventSynchronize( stop1 );
    cudaEventElapsedTime(&millisecs, start1,stop1); 
    printf("Execution time (Sequential): %.2f ms\n", millisecs);






//---------------------------------------------------------------CUDA Streams------------------------------------------------------------------
//...CUDA Kernel Launch
 

 if (err != cudaSuccess)
 {
 fprintf(stderr, "Failed to launch convert_RGB_to_Grayscale kernel (error code %s)!\n",cudaGetErrorString(err));
 exit(EXIT_FAILURE);
 }

//......Write image Buffer to output file.......
 fwrite(h_B,sizeof(unsigned char), imgSize,fOut);
err = cudaFree(d_A0);
err = cudaFree(d_B0);
err = cudaFree(d_A1);
err = cudaFree(d_B1);
err = cudaFree(d_A2);
err = cudaFree(d_B2);
err = cudaFree(d_A3);
err = cudaFree(d_B3);


free(host_Buffer);
free(h_B);
 if (err != cudaSuccess)
 {
 fprintf(stderr, "Failed to free device Buffer (error code %s)!\n",cudaGetErrorString(err));
 exit(EXIT_FAILURE);
 }
 err = cudaDeviceReset();
 if (err != cudaSuccess)
 {
 fprintf(stderr, "Failed to deinitialize the device! error=%s\n", cudaGetErrorString(err));
 exit(EXIT_FAILURE);
 }
   //fclose(fIn);
   //fclose(fOut);

}



int main(void)
{
 
   
   
    
    FILE *fInput = fopen("images/lena_color.bmp","rb");
    FILE  *fOutput = fopen("images/lena_gray.bmp","wb");
    apply_negative(fInput,fOutput);

    fclose(fInput);
    fclose(fOutput);
 
	
   
   printf("Success\n");
    


 return 0;
}
